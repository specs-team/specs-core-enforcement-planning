package eu.specs.project.enforcement.planning.core.repository;

import eu.specs.datamodel.enforcement.SupplyChainActivity;
import eu.specs.project.enforcement.planning.core.TestParent;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:planning-core-context-test.xml")
public class SCActivityRepositoryTest extends TestParent {

    @Autowired
    private SCActivityRepository scaRepository;

    @Test
    public void test() throws Exception {
        SupplyChainActivity sca = new SupplyChainActivity();
        sca.setId("SCA_ID");
        scaRepository.save(sca);

        SupplyChainActivity sca1 = scaRepository.findById(sca.getId());
        assertEquals(sca1.getId(), sca.getId());

        scaRepository.delete(sca);
        assertNull(scaRepository.findById(sca.getId()));
    }
}