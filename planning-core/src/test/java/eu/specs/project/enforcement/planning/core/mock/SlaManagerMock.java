package eu.specs.project.enforcement.planning.core.mock;

import com.google.common.io.Resources;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.Charset;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

@Component
public class SlaManagerMock {

    public void init() throws IOException {
        String slaXml =
                Resources.toString(this.getClass().getResource("/SLATemplate-WebPool-SVA.xml"), Charset.forName("UTF-8"));

        stubFor(get(urlPathEqualTo("/sla-manager/cloud-sla/slas/56BB164F001A8A2CA67C7AAF"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "text/xml")
                        .withBody(slaXml)));

        stubFor(post(urlPathMatching("/sla-manager/cloud-sla/slas/[\\w-]+/(observe|terminate)"))
                .willReturn(aResponse()
                        .withStatus(200)));
    }
}
