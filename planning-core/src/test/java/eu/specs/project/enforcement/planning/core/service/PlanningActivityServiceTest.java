package eu.specs.project.enforcement.planning.core.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.datamodel.enforcement.PlanningActivity;
import eu.specs.datamodel.enforcement.SupplyChain;
import eu.specs.datamodel.enforcement.SupplyChainActivity;
import eu.specs.project.enforcement.planning.core.TestParent;
import eu.specs.project.enforcement.planning.core.mock.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:planning-core-context-test.xml")
public class PlanningActivityServiceTest extends TestParent {
    private static final Logger logger = LogManager.getLogger(PlanningActivityServiceTest.class);

    @Autowired
    private PlanningActivityService paService;

    @Autowired
    private SCActivityService scActivityService;

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(Integer.parseInt(System.getProperty("wiremock.port")));

    @Test
    public void testService() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        new ServiceManagerMock().init();
        new ImplementationMock().init();
        new SlaManagerMock().init();
        new MonipoliMock().init();
        new CtpMock().init();

        JAXBContext jaxbContext = JAXBContext.newInstance(AgreementOffer.class);
        Unmarshaller u = jaxbContext.createUnmarshaller();
        AgreementOffer agreementOffer = (AgreementOffer) u.unmarshal(
                SCActivityServiceTest.class.getResourceAsStream("/SLATemplate-WebPool-SVA.xml"));
        logger.trace("Agreement offer deserialized successfully.");

        SupplyChainActivity scaInput = objectMapper.readValue(
                this.getClass().getResourceAsStream("/sca-input-data.json"), SupplyChainActivity.class);

        SupplyChainActivity sca = scActivityService.buildSupplyChains(scaInput);

        assertNotNull(sca.getId());
        assertEquals(sca.getState(), SupplyChainActivity.Status.COMPLETED);

        List<SupplyChain> supplyChains = scActivityService.getSupplyChains(sca);
        logger.trace("Number of supply chains: " + supplyChains.size());
        SupplyChain supplyChain1 = supplyChains.get(0);
        logger.trace("Supply chain 1 ID: " + supplyChain1.getId());
        assertEquals(supplyChain1.getSecurityMechanisms().size(), 2);
        assertEquals(supplyChain1.getSecurityMechanisms().get(0), "SM1");
        assertEquals(supplyChain1.getSecurityMechanisms().get(1), "SM2");

        PlanningActivity planningActivity = paService.createPlanningActivity(supplyChain1);
        Date startTime = new Date();
        while (planningActivity.getState() != PlanningActivity.Status.ACTIVE &&
                planningActivity.getState() != PlanningActivity.Status.ERROR) {
            Thread.sleep(1000);
            if (new Date().getTime() - startTime.getTime() > 15000) {
                fail("Timeout waiting for planning activity to finish.");
            }
        }

        assertEquals(planningActivity.getState(), PlanningActivity.Status.ACTIVE);
        assertEquals(planningActivity.getSlaId(), agreementOffer.getName());
        assertEquals(planningActivity.getSupplyChain().getId(), supplyChain1.getId());
        assertEquals(planningActivity.getSupplyChainId(), supplyChain1.getId());
        assertEquals(planningActivity.getPlanCount(), 2);
    }
}