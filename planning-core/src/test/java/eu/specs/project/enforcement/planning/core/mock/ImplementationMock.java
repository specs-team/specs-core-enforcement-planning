package eu.specs.project.enforcement.planning.core.mock;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.specs.datamodel.enforcement.ImplActivity;

import java.io.IOException;
import java.util.UUID;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

public class ImplementationMock {

    private ObjectMapper objectMapper;

    public ImplementationMock() throws Exception {
        objectMapper = new ObjectMapper();
    }

    public void init() throws IOException {

        ImplActivity implActivity = new ImplActivity();
        implActivity.setId(UUID.randomUUID().toString());

        stubFor(post(urlEqualTo("/implementation-api/impl-activities"))
                .withHeader("Content-Type", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(201)
                        .withHeader("Content-Type", "application/json")
                        .withBody(objectMapper.writeValueAsString(implActivity))));

        stubFor(post(urlEqualTo("/implementation-api/impl-plans"))
                .withHeader("Content-Type", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(201)));

        ImplActivity implActivity1 = new ImplActivity();
        implActivity1.setId(implActivity.getId());
        implActivity1.setState(ImplActivity.Status.ACTIVE);
        implActivity1.setNewPlanId(UUID.randomUUID().toString());

        stubFor(get(urlPathMatching("/implementation-api/impl-activities/[\\w-]+"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(objectMapper.writeValueAsString(implActivity1))));

        stubFor(get(urlPathMatching("/implementation-api/impl-activities/[\\w-]+/status"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "text/plain")
                        .withBody("ACTIVE")));

        stubFor(delete(urlPathMatching("/implementation-api/impl-activities/[\\w-]+"))
                .willReturn(aResponse()
                        .withStatus(200)));
    }
}
