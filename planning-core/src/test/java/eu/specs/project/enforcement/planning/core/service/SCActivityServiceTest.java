package eu.specs.project.enforcement.planning.core.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.datamodel.enforcement.SupplyChain;
import eu.specs.datamodel.enforcement.SupplyChainActivity;
import eu.specs.project.enforcement.planning.core.TestParent;
import eu.specs.project.enforcement.planning.core.mock.ServiceManagerMock;
import eu.specs.project.enforcement.planning.core.mock.SlaManagerMock;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:planning-core-context-test.xml")
public class SCActivityServiceTest extends TestParent {
    private static final Logger logger = LogManager.getLogger(SCActivityServiceTest.class);

    @Autowired
    SCActivityService scActivityService;

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(Integer.parseInt(System.getProperty("wiremock.port")));

    @Test
    public void testService() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        new ServiceManagerMock().init();
        new SlaManagerMock().init();

        JAXBContext jaxbContext = JAXBContext.newInstance(AgreementOffer.class);
        Unmarshaller u = jaxbContext.createUnmarshaller();
        AgreementOffer agreementOffer = (AgreementOffer) u.unmarshal(
                SCActivityServiceTest.class.getResourceAsStream("/SLATemplate-WebPool-SVA.xml"));
        logger.trace("Agreement offer deserialized successfully.");

        SupplyChainActivity scaInput = objectMapper.readValue(
                this.getClass().getResourceAsStream("/sca-input-data.json"), SupplyChainActivity.class);

        SupplyChainActivity sca = scActivityService.buildSupplyChains(scaInput);

        long startTime = System.currentTimeMillis();
        while (!scActivityService.isFinished(sca) && (System.currentTimeMillis() - startTime) < 10000) {
            Thread.sleep(100);
        }
        assertTrue(scActivityService.isFinished(sca));

        assertNotNull(sca.getId());
        assertEquals(sca.getState(), SupplyChainActivity.Status.COMPLETED);

        List<SupplyChain> supplyChains = scActivityService.getSupplyChains(sca);
        for (SupplyChain supplyChain : supplyChains) {
            logger.trace("Supply chain ID: " + supplyChain.getId());
            assertEquals(supplyChain.getSecurityMechanisms().size(), 2);
            assertEquals(supplyChain.getSecurityMechanisms().get(0), "SM1");
            assertEquals(supplyChain.getSecurityMechanisms().get(1), "SM2");
            assertEquals(supplyChain.getCloudResource().getProviderId(), "ec2");
        }
    }
}
