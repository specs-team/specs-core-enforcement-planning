package eu.specs.project.enforcement.planning.core.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import eu.specs.datamodel.enforcement.PlanningActivity;
import eu.specs.datamodel.enforcement.Reconfiguration;
import eu.specs.project.enforcement.planning.core.TestParent;
import eu.specs.project.enforcement.planning.core.mock.ImplementationMock;
import eu.specs.project.enforcement.planning.core.mock.MonipoliMock;
import eu.specs.project.enforcement.planning.core.mock.SlaManagerMock;
import eu.specs.project.enforcement.planning.core.repository.PlanningActivityRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:planning-core-context-test.xml")
public class ReconfigServiceTest extends TestParent {
    private static final Logger logger = LogManager.getLogger(ReconfigServiceTest.class);

    private static String SLA_ID = "56BB164F001A8A2CA67C7AAF";

    @Autowired
    private ReconfigService reconfigService;

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(Integer.parseInt(System.getProperty("wiremock.port")));

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private PlanningActivityRepository planningActivityRepository;

    @Test
    public void testTermination() throws Exception {
        logger.debug("testTermination() started.");

        new SlaManagerMock().init();
        new ImplementationMock().init();
        new MonipoliMock().init();

        PlanningActivity planningActivity = objectMapper.readValue(
                this.getClass().getResourceAsStream("/planning-activity.json"), PlanningActivity.class);
        planningActivityRepository.save(planningActivity);

        Reconfiguration reconfigData = new Reconfiguration();
        reconfigData.setSlaId(SLA_ID);
        reconfigData.setLabel("TERMINATE");

        Reconfiguration reconfig = reconfigService.reconfigure(reconfigData);

        assertNotNull(reconfig.getId());

        assertNotNull(reconfigService.findById(reconfig.getId()));

        ReconfigService.ReconfigsFilter filter = new ReconfigService.ReconfigsFilter();
        filter.setSlaId(SLA_ID);

        assertEquals(reconfigService.findAll(filter).size(), 1);
    }
}