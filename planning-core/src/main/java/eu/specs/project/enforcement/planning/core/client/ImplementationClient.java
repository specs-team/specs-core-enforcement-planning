package eu.specs.project.enforcement.planning.core.client;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

import eu.specs.datamodel.enforcement.ImplActivity;
import eu.specs.datamodel.enforcement.ImplementationPlan;
import eu.specs.project.enforcement.planning.core.exceptions.PlanningException;
import eu.specs.project.enforcement.planning.core.util.AppConfig;
import eu.specs.project.enforcement.planning.core.util.JsonDumper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
public class ImplementationClient {
    private static String IMPL_PLAN_PATH = "/impl-plans/{id}";
    private static String IMPL_ACT_PATH = "/impl-activities/{id}";
    private static final Logger logger = LogManager.getLogger(ImplementationClient.class);

    private WebTarget implementationTarget;

    @Autowired
    public ImplementationClient(AppConfig appConfig) {
        Client client = ClientBuilder.newBuilder()
                .register(JacksonJsonProvider.class)
                .build();

        implementationTarget = client.target(appConfig.getImplementationApiAddress());
    }

    public ImplActivity createImplementationActivity(ImplementationPlan implPlan) throws PlanningException {
        try {
            logger.debug("Creating implementation activity at Implementation for the implementation plan {}...",
                    implPlan.getId());
            ImplActivity implActivity = implementationTarget
                    .path("impl-activities")
                    .request(MediaType.APPLICATION_JSON)
                    .post(Entity.json(implPlan), ImplActivity.class);

            if (logger.isTraceEnabled()) {
                logger.trace("Implementation activity created:\n{}", JsonDumper.dump(implActivity));
            }
            return implActivity;

        } catch (Exception e) {
            throw new PlanningException(
                    "Failed to create implementation activity at Implementation: " + e.getMessage(), e);
        }
    }

    public void reconfigureImplementation(String implActivityId, ImplementationPlan implPlan)
            throws PlanningException {
        try {
            logger.debug("Launching reconfiguration implementation activity at Implementation for the implementation plan {}...",
                    implPlan.getId());
            implementationTarget
                    .path(IMPL_ACT_PATH)
                    .resolveTemplate("id", implActivityId)
                    .request()
                    .put(Entity.json(implPlan));

            logger.debug("Reconfiguration implementation activity launched successfully.");

        } catch (Exception e) {
            throw new PlanningException(
                    "Failed to launch reconfiguration implementation activity at Implementation: " + e.getMessage(), e);
        }
    }

    public ImplementationPlan getImplPlan(String implPlanId) throws PlanningException {
        try {
            logger.debug("Retrieving implementation plan {}...", implPlanId);
            ImplementationPlan implPlan = implementationTarget
                    .path(IMPL_PLAN_PATH)
                    .resolveTemplate("id", implPlanId)
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .get(ImplementationPlan.class);

            logger.debug("Implementation plan retrieved successfully.");
            return implPlan;
        } catch (NotFoundException e) {
            logger.error(String.format("Implementation plan %s cannot be found.", implPlanId), e);
            return null;
        } catch (Exception e) {
            throw new PlanningException("Failed to retrieve implementation plan: " + e.getMessage(), e);
        }
    }

    public ImplActivity retrieveImplActivity(String implActivityId) throws PlanningException {
        try {
            logger.debug("Retrieving implementation activity {}...", implActivityId);

            ImplActivity implActivity = implementationTarget
                    .path(IMPL_ACT_PATH)
                    .resolveTemplate("id", implActivityId)
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .get(ImplActivity.class);

            logger.debug("Implementation activity retrieved successfully.");
            return implActivity;

        } catch (Exception e) {
            throw new PlanningException("Failed to retrieve implementation activity: " + e.getMessage(), e);
        }
    }

    public ImplActivity.Status retrieveImplActivityStatus(String implActivityId) throws PlanningException {
        try {
            String statusString = implementationTarget
                    .path(IMPL_ACT_PATH + "/status")
                    .resolveTemplate("id", implActivityId)
                    .request(MediaType.TEXT_PLAIN_TYPE)
                    .get(String.class);

            ImplActivity.Status status = ImplActivity.Status.valueOf(statusString);
            return status;

        } catch (Exception e) {
            throw new PlanningException("Failed to retrieve implementation activity status: " + e.getMessage(), e);
        }
    }

    public void deleteImplActivity(String implActId) throws PlanningException {
        try {
            logger.debug("Deleting implementation activity {}...", implActId);
            Response response = implementationTarget
                    .path(IMPL_ACT_PATH)
                    .resolveTemplate("id", implActId)
                    .request()
                    .delete();
            
            if(response==null){
            	logger.debug("The response to DELETE {}  is null",IMPL_ACT_PATH);
            }
            else if (response.getStatus()!=204){
            	logger.debug("The response was to DELETE {} failed",IMPL_ACT_PATH);
            	logger.debug("response: "+response.toString());
            }
            else{
                logger.debug("Implementation activity has been deleted successfully.");
                logger.debug("response: "+response.toString());
            }

            logger.debug("Implementation activity has been deleted successfully.");

        } catch (Exception e) {
            throw new PlanningException(
                    "Failed to delete implementation activity: " + e.getMessage(), e);
        }
    }
}
