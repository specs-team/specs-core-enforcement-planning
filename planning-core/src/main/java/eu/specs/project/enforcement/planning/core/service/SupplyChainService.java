package eu.specs.project.enforcement.planning.core.service;

import eu.specs.datamodel.enforcement.SupplyChain;
import eu.specs.project.enforcement.planning.core.repository.SupplyChainRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SupplyChainService {
    private static final Logger logger = LogManager.getLogger(SupplyChainService.class);

    @Autowired
    private SupplyChainRepository supplyChainRepository;

    public SupplyChain getSupplyChain(String scId) {
        return supplyChainRepository.find(scId);
    }

    public List<SupplyChain> findBySlaId(String slaId) {
        if (slaId != null) {
            return supplyChainRepository.findBySlaId(slaId);
        } else {
            return supplyChainRepository.getAll();
        }
    }

    public void delete(SupplyChain supplyChain) {
        logger.debug("Deleting supply chain {}...", supplyChain.getId());
        supplyChainRepository.delete(supplyChain);
    }
}
