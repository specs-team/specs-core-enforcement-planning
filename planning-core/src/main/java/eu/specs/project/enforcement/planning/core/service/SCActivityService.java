package eu.specs.project.enforcement.planning.core.service;

import eu.specs.datamodel.enforcement.SupplyChain;
import eu.specs.datamodel.enforcement.SupplyChainActivity;
import eu.specs.project.enforcement.planning.core.processor.SCActivityProcessor;
import eu.specs.project.enforcement.planning.core.repository.SCActivityRepository;
import eu.specs.project.enforcement.planning.core.repository.SupplyChainRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class SCActivityService {
    private static final Logger logger = LogManager.getLogger(SCActivityService.class);

    @Autowired
    private SCActivityRepository scaRepository;

    @Autowired
    private SupplyChainRepository supplyChainRepository;

    @Autowired
    private SCActivityProcessor scActivityProcessor;

    public SupplyChainActivity buildSupplyChains(final SupplyChainActivity sca) {

        sca.setId(UUID.randomUUID().toString());
        sca.setCreationTime(new Date());
        sca.setState(SupplyChainActivity.Status.CREATED);
        scaRepository.save(sca);

        scActivityProcessor.buildSupplyChains(sca);

        return sca;
    }

    public boolean isFinished(SupplyChainActivity sca) {
        return sca.getState() == SupplyChainActivity.Status.COMPLETED ||
                sca.getState() == SupplyChainActivity.Status.ERROR;
    }

    public SupplyChainActivity getSCA(String scaId) {
        return scaRepository.findById(scaId);
    }

    public List<SupplyChainActivity> getAll() {
        return scaRepository.getAll();
    }

    public void deleteSCA(SupplyChainActivity sca) {
        logger.debug("Deleting supply chain activity {}...", sca.getId());
        scaRepository.delete(sca);
    }

    public List<SupplyChain> getSupplyChains(SupplyChainActivity sca) {
        return supplyChainRepository.findByScaId(sca.getId());
    }
}
