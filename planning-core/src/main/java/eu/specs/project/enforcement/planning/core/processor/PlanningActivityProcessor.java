package eu.specs.project.enforcement.planning.core.processor;

import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.datamodel.agreement.slo.ServiceProperties;
import eu.specs.datamodel.agreement.slo.Variable;
import eu.specs.datamodel.agreement.terms.GuaranteeTerm;
import eu.specs.datamodel.agreement.terms.Term;
import eu.specs.datamodel.common.Annotation;
import eu.specs.datamodel.common.SlaState;
import eu.specs.datamodel.enforcement.*;
import eu.specs.datamodel.sla.sdt.OneOpOperator;
import eu.specs.datamodel.sla.sdt.SLOType;
import eu.specs.datamodel.sla.sdt.SLOexpressionType;
import eu.specs.datamodel.sla.sdt.WeightType;
import eu.specs.project.enforcement.planning.core.client.CtpClient;
import eu.specs.project.enforcement.planning.core.client.ImplementationClient;
import eu.specs.project.enforcement.planning.core.client.MonipoliClient;
import eu.specs.project.enforcement.planning.core.client.SlaManagerClient;
import eu.specs.project.enforcement.planning.core.exceptions.PlanningException;
import eu.specs.project.enforcement.planning.core.repository.PlanningActivityRepository;
import eu.specs.project.enforcement.planning.core.repository.SCActivityRepository;
import eu.specs.project.enforcement.planning.core.util.AppConfig;
import eu.specs.project.enforcement.planning.core.util.JsonDumper;
import eu.specs.project.enforcement.planning.core.util.ServiceManagerClient;
import eu.specsproject.core.slaplatform.auditing.client.AuditClient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class PlanningActivityProcessor {
    private static final Logger logger = LogManager.getLogger(PlanningActivityProcessor.class);

    @Autowired
    private PlanningActivityRepository paRepository;

    @Autowired
    private SCActivityRepository scaRepository;

    @Autowired
    private AppConfig appConfig;

    @Autowired
    private ServiceManagerClient serviceManagerClient;

    @Autowired
    private SlaManagerClient slaManagerClient;

    @Autowired
    private CtpClient ctpClient;

    @Autowired
    private MonipoliClient monipoliClient;

    @Autowired
    private ImplementationClient implementationClient;

    @Autowired
    private AuditClient auditClient;

    @Value("${monitoring-core.ip}")
    private String monitoringCoreIp;

    @Value("${monitoring-core.port}")
    private int monitoringCorePort;

    public void processPlanningActivity(PlanningActivity planningActivity) {
        logger.debug("processPlanningActivity() started. Planning activity ID: {}, SLA ID: {}", planningActivity.getId(), planningActivity.getSlaId());
        try {
            planningActivity.setState(PlanningActivity.Status.BUILDING);
            paRepository.save(planningActivity);

            processPlanningActivityImpl(planningActivity);

        } catch (Exception e) {
            logger.error("Planning activity failed: " + e.getMessage(), e);
            planningActivity.setState(PlanningActivity.Status.ERROR);
            planningActivity.addAnnotation(new Annotation("error", e.getMessage(), new Date()));
            paRepository.save(planningActivity);
        }
    }

    public void processReconfigPlanningActivity(PlanningActivity planningActivity, ImplementationPlan implPlan) {
        logger.debug("processReconfigPlanningActivity() started for planning activity {} and implementation plan {}",
                planningActivity.getId(), implPlan.getId());
        try {
            processReconfigPlanningActivityImpl(planningActivity, implPlan);

        } catch (Exception e) {
            logger.error("Reconfiguration planning activity failed: " + e.getMessage(), e);
            planningActivity.setState(PlanningActivity.Status.ERROR);
            planningActivity.addAnnotation(new Annotation("error", e.getMessage(), new Date()));
            paRepository.save(planningActivity);
        }
    }

    private void processPlanningActivityImpl(PlanningActivity planningActivity)
            throws PlanningException, InterruptedException {

        long startTimePA = System.currentTimeMillis();

        auditClient.logComponentActivity("Planning", CompActivity.ComponentState.ACTIVATED,
                planningActivity.getSlaId(), SlaState.SIGNED);

        ImplementationPlan implPlan = createImplementationPlan(planningActivity);
        if (logger.isTraceEnabled()) {
            logger.trace("Implementation plan:\n{}", JsonDumper.dump(implPlan));
        }

        planningActivity.setActivePlanId(implPlan.getId());
        planningActivity.setPlanCount(1);
        paRepository.save(planningActivity);

        // implement plan (create implementation activity)
        ImplActivity implActivity = implementationClient.createImplementationActivity(implPlan);

        planningActivity.setImplActivityId(implActivity.getId());
        planningActivity.setState(PlanningActivity.Status.IMPLEMENTING);
        paRepository.save(planningActivity);

        // wait until the implementation activity finishes
        logger.debug("Waiting for the implementation activity {} to finish...", implActivity.getId());
        Date startTime = new Date();
        long timeout = (long) appConfig.getImplActivityTimeout() * 1000;
        do {
            Thread.sleep(10000);
            ImplActivity.Status status = implementationClient.retrieveImplActivityStatus(implActivity.getId());
            logger.debug("Implementation activity status: {}", status);

            if (status == ImplActivity.Status.ACTIVE || status == ImplActivity.Status.ERROR) {
                break;
            }
            if (new Date().getTime() - startTime.getTime() > timeout) {
                throw new PlanningException("Timeout waiting for the implementation activity to finish.");
            }
        } while (true);

        // retrieve final implementation activity
        ImplActivity implActivityFinal = implementationClient.retrieveImplActivity(implActivity.getId());
        if (implActivityFinal.getState() == ImplActivity.Status.ERROR) {
            logger.error("Implementation activity failed.");
            String errorMsg = "N/A";
            for (Annotation annotation : implActivityFinal.getAnnotations()) {
                if (Objects.equals(annotation.getName(), "error")) {
                    errorMsg = annotation.getValue();
                    break;
                }
            }
            throw new PlanningException("Implementation activity failed: " + errorMsg);
        } else {
            long duration = (new Date().getTime() - startTime.getTime()) / 1000;
            logger.debug("Implementation activity finished successfully in {} seconds.", duration);
        }

        // create Monipoli rules
        try {
            createMonipoliRules(planningActivity, implPlan);
        } catch (Exception e) {
            logger.error("Failed to create Monipoli rules. If needed the Monipoli has to be configured manually.", e);
        }

        // set SLA state to 'Observed'
        slaManagerClient.setSlaState(planningActivity.getSlaId(), SlaState.OBSERVED);

        try {
            ctpClient.notifySlaCreation(planningActivity.getSlaId());
        } catch (PlanningException e) {
            logger.error("Failed to notify CTP. If needed the CTP has to be configured manually.", e);
        }

        planningActivity.setState(PlanningActivity.Status.ACTIVE);
        planningActivity.setActivePlanId(implActivityFinal.getNewPlanId());
        planningActivity.setPlanCount(planningActivity.getPlanCount() + 1);
        paRepository.save(planningActivity);

        // prepare list of services for ServiceActivity audit event
        Set<String> serviceActivityServices = new HashSet<>();
        for (ImplementationPlan.Pool pool : implPlan.getPools()) {
            for (ImplementationPlan.Vm vm : pool.getVms()) {
                for (ImplementationPlan.Component component : vm.getComponents()) {
                    serviceActivityServices.add(component.getId());
                }
            }
        }

        // create ServiceActivity audit event
        ServiceActivity serviceActivity = new ServiceActivity();
        serviceActivity.setId(UUID.randomUUID().toString());
        serviceActivity.setSlaId(planningActivity.getSlaId());
        serviceActivity.setCreationTime(new Date());
        serviceActivity.setImplPlanId(implActivityFinal.getId());
        serviceActivity.setSlaState(SlaState.OBSERVED);
        serviceActivity.setServices(new ArrayList<>(serviceActivityServices));
        auditClient.logServiceActivity(serviceActivity);

        auditClient.logComponentActivity("Planning", CompActivity.ComponentState.DEACTIVATED,
                planningActivity.getSlaId(), SlaState.OBSERVED);

        logger.debug("Planning activity has finished successfully in {} seconds. Status is ACTIVE.",
                (System.currentTimeMillis() - startTimePA) / 1000.0);
        if (logger.isTraceEnabled()) {
            logger.trace("Planning activity:\n{}", JsonDumper.dump(planningActivity));
        }
    }

    private void processReconfigPlanningActivityImpl(PlanningActivity planningActivity, ImplementationPlan implPlan)
            throws PlanningException, InterruptedException {

        long startTimePA = System.currentTimeMillis();

        auditClient.logComponentActivity("Planning", CompActivity.ComponentState.ACTIVATED,
                planningActivity.getSlaId(), SlaState.SIGNED);

        String implActivityId = planningActivity.getImplActivityId();

        planningActivity.setState(PlanningActivity.Status.IMPLEMENTING);
        paRepository.save(planningActivity);

        // implement reconfigured plan
        implementationClient.reconfigureImplementation(implActivityId, implPlan);

        // wait until the implementation activity finishes
        logger.debug("Waiting for the implementation activity {} to finish...", implActivityId);
        Date startTime = new Date();
        long timeout = (long) appConfig.getImplActivityTimeout() * 1000;
        do {
            Thread.sleep(10000);
            ImplActivity.Status status = implementationClient.retrieveImplActivityStatus(implActivityId);
            logger.debug("Implementation activity status: {}", status);

            if (status == ImplActivity.Status.ACTIVE || status == ImplActivity.Status.ERROR) {
                break;
            }
            if (new Date().getTime() - startTime.getTime() > timeout) {
                throw new PlanningException("Timeout waiting for the implementation activity to finish.");
            }
        } while (true);

        // retrieve final implementation activity
        ImplActivity implActivityFinal = implementationClient.retrieveImplActivity(implActivityId);
        if (implActivityFinal.getState() == ImplActivity.Status.ERROR) {
            logger.error("Implementation activity failed.");
            String errorMsg = "N/A";
            for (Annotation annotation : implActivityFinal.getAnnotations()) {
                if (Objects.equals(annotation.getName(), "error")) {
                    errorMsg = annotation.getValue();
                    break;
                }
            }
            throw new PlanningException("Implementation activity failed: " + errorMsg);
        } else {
            long duration = (new Date().getTime() - startTime.getTime()) / 1000;
            logger.debug("Implementation activity finished successfully in {} seconds.", duration);
        }

        // update Monipoli rules
        try {
            createMonipoliRules(planningActivity, implPlan);
        } catch (Exception e) {
            logger.error("Failed to create Monipoli rules. If needed the Monipoli has to be configured manually.", e);
        }

        // set SLA state to 'Observed'
        slaManagerClient.setSlaState(planningActivity.getSlaId(), SlaState.OBSERVED);

        planningActivity.setState(PlanningActivity.Status.ACTIVE);
        paRepository.save(planningActivity);

        // prepare list of services for ServiceActivity audit event
        Set<String> serviceActivityServices = new HashSet<>();
        for (ImplementationPlan.Pool pool : implPlan.getPools()) {
            for (ImplementationPlan.Vm vm : pool.getVms()) {
                if (vm.getComponents() != null) {
                    for (ImplementationPlan.Component component : vm.getComponents()) {
                        serviceActivityServices.add(component.getId());
                    }
                }
            }
        }

        // create ServiceActivity audit event
        ServiceActivity serviceActivity = new ServiceActivity();
        serviceActivity.setId(UUID.randomUUID().toString());
        serviceActivity.setSlaId(planningActivity.getSlaId());
        serviceActivity.setCreationTime(new Date());
        serviceActivity.setImplPlanId(implActivityFinal.getId());
        serviceActivity.setSlaState(SlaState.OBSERVED);
        serviceActivity.setServices(new ArrayList<>(serviceActivityServices));
        auditClient.logServiceActivity(serviceActivity);

        auditClient.logComponentActivity("Planning", CompActivity.ComponentState.DEACTIVATED,
                planningActivity.getSlaId(), SlaState.OBSERVED);

        logger.debug("Reconfiguration planning activity has finished successfully in {} seconds. Status is ACTIVE.",
                (System.currentTimeMillis() - startTimePA) / 1000.0);
        if (logger.isTraceEnabled()) {
            logger.trace("Planning activity:\n{}", JsonDumper.dump(planningActivity));
        }
    }

    private ImplementationPlan createImplementationPlan(PlanningActivity planningActivity) throws PlanningException {
        SupplyChain supplyChain = planningActivity.getSupplyChain();

        ImplementationPlan implPlan = new ImplementationPlan();
        implPlan.setId(UUID.randomUUID().toString());
        implPlan.setSlaId(planningActivity.getSlaId());
        implPlan.setPlanningActivityId(planningActivity.getId());
        implPlan.setSupplyChainId(planningActivity.getSupplyChainId());
        implPlan.setCreationTime(new Date());
        implPlan.setMonitoringCoreIp(monitoringCoreIp);
        implPlan.setMonitoringCorePort(monitoringCorePort);

        ImplementationPlan.Iaas iaas = new ImplementationPlan.Iaas();
        iaas.setProvider(supplyChain.getCloudResource().getProviderId());
        iaas.setZone(supplyChain.getCloudResource().getZoneId());
        iaas.setAppliance(supplyChain.getCloudResource().getAppliance());
        iaas.setHardware(supplyChain.getCloudResource().getVmType());
        implPlan.setIaas(iaas);

        SupplyChainActivity sca = scaRepository.findById(supplyChain.getScaId());
        implPlan.setSlos(sca.getSlos());

        // prepare set of all metrics referenced in SLOs
        Set<String> metricsReferencedInSlos = new HashSet<>();
        for (Slo slo : sca.getSlos()) {
            metricsReferencedInSlos.add(slo.getMetricId());
        }

        Map<String, eu.specs.datamodel.enforcement.Component> componentsMap = new HashMap<>();

        for (String mechanismId : supplyChain.getSecurityMechanisms()) {
            SecurityMechanism securityMechanism = serviceManagerClient.retrieveSecurityMechanism(mechanismId);
            if (securityMechanism.getMetadata().getComponents() != null) {
                for (eu.specs.datamodel.enforcement.Component component : securityMechanism.getMetadata().getComponents()) {
                    componentsMap.put(component.getName(), component);
                }
            }

            for (Measurement measurement : securityMechanism.getMeasurements()) {
                for (String measurementMetric : measurement.getMetrics()) {
                    if (metricsReferencedInSlos.contains(measurementMetric)) {
                        implPlan.addMeasurement(measurement);
                        break;
                    }
                }
            }
        }

        Map<Integer, ImplementationPlan.Pool> poolsMap = new LinkedHashMap<>();

        for (SupplyChain.Allocation allocation : supplyChain.getAllocations()) {
            ImplementationPlan.Pool pool;
            int poolSeqNum = allocation.getPoolSeqNum();
            if (poolsMap.containsKey(poolSeqNum)) {
                pool = poolsMap.get(poolSeqNum);
            } else {
                pool = new ImplementationPlan.Pool();
                pool.setPoolSeqNum(poolSeqNum);
                poolsMap.put(poolSeqNum, pool);
            }

            ImplementationPlan.Vm vm = new ImplementationPlan.Vm();
            vm.setVmSeqNum(allocation.getVmSeqNum());
            pool.addVm(vm);

            for (String componentId : allocation.getComponentIds()) {
                ImplementationPlan.Component component = new ImplementationPlan.Component();
                if (!componentsMap.containsKey(componentId)) {
                    throw new PlanningException("No data available for the component " + componentId);
                }
                // component data from security mechanism
                eu.specs.datamodel.enforcement.Component componentSM = componentsMap.get(componentId);

                component.setAcquirePublicIp(componentSM.getVmRequirement().isAcquirePublicIp());
                component.setCookbook(componentSM.getCookbook());
                component.setFirewall(componentSM.getVmRequirement().getFirewall());
                component.setImplementationStep(componentSM.getImplementationStep());
                component.setId(componentSM.getName());
                component.setPrivateIpsCount(componentSM.getVmRequirement().getPrivateIpsCount());
                component.setRecipe(componentSM.getRecipe());
                vm.addComponent(component);
            }
        }

        for (Map.Entry<Integer, ImplementationPlan.Pool> entry : poolsMap.entrySet()) {
            implPlan.addPool(entry.getValue());
        }

        return implPlan;
    }

    private void createMonipoliRules(PlanningActivity planningActivity, ImplementationPlan implPlan)
            throws PlanningException {
        logger.debug("Creating Monipoli rules...");
        AgreementOffer sla = slaManagerClient.retrieveSla(planningActivity.getSlaId());
        Pattern termPattern = Pattern.compile("//specs:capability\\[@id='([^']+)'\\]");

        // metric name -> SLO mapping
        Map<String, Slo> metricSloMapping = new HashMap<>();
        for (Slo slo : implPlan.getSlos()) {
            metricSloMapping.put(slo.getMetricId(), slo);
        }

        // metric name -> capability name mapping
        Map<String, String> metricCapabilityMapping = new HashMap<>();
        // capability -> ServiceProperties mapping
        Map<String, ServiceProperties> capabilityServicePropertiesMapping = new HashMap<>();

        for (Term term : sla.getTerms().getAll().getAll()) {
            if (term instanceof ServiceProperties) {
                ServiceProperties serviceProperties = (ServiceProperties) term;
                // extract capability name from the guarantee term name
                Matcher m = termPattern.matcher(serviceProperties.getName());
                String capability;
                if (m.find()) {
                    capability = m.group(1);
                } else {
                    throw new PlanningException(
                            "Failed to extract capability from the guarantee term name: " + serviceProperties.getName());
                }

                capabilityServicePropertiesMapping.put(capability, serviceProperties);

                for (Variable variable : serviceProperties.getVariableSet().getVariables()) {
                    metricCapabilityMapping.put(variable.getMetric(), capability);
                }
            }
        }

        // capability -> GuaranteeTerm mapping
        Map<String, GuaranteeTerm> capabilityGuaranteeTermMapping = new HashMap<>();
        for (Term term : sla.getTerms().getAll().getAll()) {
            if (term instanceof GuaranteeTerm) {
                GuaranteeTerm guaranteeTerm = (GuaranteeTerm) term;
                // extract capability name from the guarantee term name
                Matcher m = termPattern.matcher(guaranteeTerm.getName());
                String capability;
                if (m.find()) {
                    capability = m.group(1);
                } else {
                    throw new PlanningException(
                            "Failed to extract capability from the guarantee term name: " + guaranteeTerm.getName());
                }
                capabilityGuaranteeTermMapping.put(capability, guaranteeTerm);
            }
        }

        // add SLOs for measurements to the SLA which will be used as input for the Monipoli
        for (Measurement measurement : implPlan.getMeasurements()) {
            String operator = measurement.getMonitoringEvent().getCondition().getOperator();
            String threshold = measurement.getMonitoringEvent().getCondition().getThreshold();
            String thresholdValue;
            if (threshold.startsWith("metric:")) { // threshold is metric reference
                String thresholdMetric = threshold.substring("metric:".length());
                if (!metricSloMapping.containsKey(thresholdMetric)) {
                    throw new PlanningException(String.format(
                            "No SLO found in the implementation plan for the metric %s specified in the measurement %s.",
                            thresholdMetric, measurement.getId()));
                }
                thresholdValue = metricSloMapping.get(thresholdMetric).getValue();
            } else { // threshold is string value
                thresholdValue = threshold;
            }

            // determine measurement importance level
            Slo.ImportanceLevel importanceLevel = null;
            for (String metricId : measurement.getMetrics()) {
                for (Slo slo : implPlan.getSlos()) {
                    if (slo.getMetricId().equals(metricId)) {
                        if (importanceLevel == null) {
                            importanceLevel = slo.getImportanceLevel();
                        } else if (importanceLevel.compareTo(slo.getImportanceLevel()) > 0) {
                            importanceLevel = slo.getImportanceLevel();
                        }
                    }
                }
            }

            String measurementVarName = measurement.getId() + "_var";
            SLOType slo = new SLOType();
            slo.setSLOID(measurement.getId() + "_slo");
            slo.setMetricREF(measurementVarName);
            slo.setImportanceWeight(WeightType.valueOf(importanceLevel.name()));
            SLOexpressionType slOexpression = new SLOexpressionType();
            // TODO: TwoOpExpression???
            SLOexpressionType.OneOpExpression oneOpExpression = new SLOexpressionType.OneOpExpression();
            OneOpOperator oneOpOperator = OneOpOperator.fromValue(operator);

            if (oneOpOperator == OneOpOperator.EQUAL &&
                    ("true".equalsIgnoreCase(threshold) || "false".equalsIgnoreCase(threshold))) {
                // we cannot use inverse operator NOT_EQUAL because MoniPoli doesn't support it
                // so we have to negate the value
                //Boolean thresholdValueNegated = !Boolean.valueOf(threshold);
                oneOpExpression.setOperand(threshold);
                oneOpExpression.setOperator(oneOpOperator);
            } else {
                oneOpExpression.setOperand(thresholdValue);
                //OneOpOperator inverseOperator = oneOpOperator.getInverse();
                oneOpExpression.setOperator(oneOpOperator);
            }

            slOexpression.setOneOpExpression(oneOpExpression);
            slo.setSLOexpression(slOexpression);

            // find appropriate GuaranteeTerm for the measurement
            String capability = metricCapabilityMapping.get(measurement.getMetrics().get(0));
            GuaranteeTerm guaranteeTerm = capabilityGuaranteeTermMapping.get(capability);
            guaranteeTerm.getServiceLevelObjective().getCustomServiceLevel().getObjectiveList().getSLO().add(slo);

            // create corresponding variable
            Variable variable = new Variable();
            variable.setName(measurementVarName);
            variable.setMetric(measurement.getId());
            variable.setLocation("");
            capabilityServicePropertiesMapping.get(capability).getVariableSet().getVariables().add(variable);
        }

        if (logger.isTraceEnabled()) {
            try {
                JAXBContext jaxbContext = JAXBContext.newInstance(AgreementOffer.class);
                Marshaller marshaller = jaxbContext.createMarshaller();
                StringWriter stringWriter = new StringWriter();
                marshaller.marshal(sla, stringWriter);
                String slaXml = stringWriter.toString();
                logger.trace("Input data for the Monipoli:\n{}", slaXml);
            } catch (JAXBException e) {
                logger.error("Failed to marshal AgreementOffer: " + e.getMessage(), e);
            }
        }

        monipoliClient.updateMonipoli(sla);

        logger.debug("Monipoli rules have been created successfully.");
    }
}
