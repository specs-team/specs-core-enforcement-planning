package eu.specs.project.enforcement.planning.core.client;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.project.enforcement.planning.core.exceptions.PlanningException;
import eu.specs.project.enforcement.planning.core.util.AppConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.client.ClientProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

@Component
public class MonipoliClient {
    private static final Logger logger = LogManager.getLogger(MonipoliClient.class);
    private WebTarget monipoliTarget;

    @Autowired
    public MonipoliClient(AppConfig appConfig) {
        Client client = ClientBuilder.newBuilder()
                .register(JacksonJsonProvider.class)
                .build();

        monipoliTarget = client.target(appConfig.getMonipoliAddress());
    }

    public void updateMonipoli(AgreementOffer sla) throws PlanningException {
        try {
            logger.debug("Sending request to Monipoli...");
            Response response = monipoliTarget
                    .path("/monipoli")
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .post(Entity.xml(sla));

            if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
                throw new PlanningException("Invalid response from Monipoli: " + response.getStatusInfo().toString());
            } else {
                logger.debug("Success: " + response.getStatusInfo().toString());
            }
        } catch (Exception e) {
            throw new PlanningException(String.format(
                    "Failed to create Monipoli rules for the SLA %s: %s", sla.getName(), e.getMessage()), e);
        }
    }

    public void removeMonipoliRules(String slaId) throws PlanningException {
        try {
            logger.debug("Sending request to Monipoli to remove rules for SLA ID {}...", slaId);
            Map<String, String> content = new HashMap<>();
            content.put("sla_id", slaId);
            Response response = monipoliTarget
                    // HTTP DELETE method shouldn't have a body according to the specification
                    // we have to suppress validation otherwise an error is thrown
                    .property(ClientProperties.SUPPRESS_HTTP_COMPLIANCE_VALIDATION, true)
                    .path("/monipoli")
                    .request()
                    .method("DELETE", Entity.json(content));

            if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
                throw new PlanningException("Invalid response from Monipoli: " + response.getStatusInfo().toString());
            } else {
                logger.debug("Monipoli rules have been removed successfully.");
            }
        } catch (Exception e) {
            throw new PlanningException("Request to the Monipoli failed: " + e.getMessage(), e);
        }
    }
}
