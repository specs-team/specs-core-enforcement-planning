package eu.specs.project.enforcement.planning.core.client;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.datamodel.common.SlaState;
import eu.specs.project.enforcement.planning.core.exceptions.PlanningException;
import eu.specs.project.enforcement.planning.core.util.AppConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

@Component
public class SlaManagerClient {
    private static String SLA_PATH = "/slas/{id}";
    private static final Logger logger = LogManager.getLogger(SlaManagerClient.class);

    private WebTarget slaManagerTarget;
    private Map<SlaState, String> slaStateCommands;

    @Autowired
    public SlaManagerClient(AppConfig appConfig) {
        Client client = ClientBuilder.newBuilder()
                .register(JacksonJsonProvider.class)
                .build();

        slaManagerTarget = client.target(appConfig.getSlaManagerApiAddress());

        slaStateCommands = new HashMap<>();
        slaStateCommands.put(SlaState.OBSERVED, "observe");
        slaStateCommands.put(SlaState.TERMINATED, "terminate");
    }

    public AgreementOffer retrieveSla(String slaId) throws PlanningException {
        try {
            logger.debug("Retrieving SLA {}...", slaId);
            AgreementOffer sla = slaManagerTarget
                    .path(SLA_PATH)
                    .resolveTemplate("id", slaId)
                    .request(MediaType.TEXT_XML_TYPE)
                    .get(AgreementOffer.class);
            logger.debug("SLA {} has been retrieved successfully.", slaId);
            return sla;
        } catch (Exception e) {
            throw new PlanningException(String.format("Failed to retrieve SLA %s: %s",
                    slaId, e.getMessage()), e);
        }
    }

    public void setSlaState(String slaId, SlaState slaState) throws PlanningException {
        logger.debug("Setting SLA {} state to {}...", slaId, slaState);
        String command = slaStateCommands.get(slaState);
        if (command == null) {
            throw new UnsupportedOperationException("Invalid SLA state: " + slaState);
        }

        try {
            Response response = slaManagerTarget
                    .path(SLA_PATH + "/" + slaStateCommands.get(slaState))
                    .resolveTemplate("id", slaId)
                    .request()
                    .post(Entity.json(null));

            if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
                String content = response.readEntity(String.class);
                logger.error("Failed to set SLA state: {}\n{}", response.getStatusInfo(), content);
                throw new PlanningException(String.format("Failed to set SLA state to %s.", slaState));
            } else {
                logger.debug("SLA state set successfully.");
            }

        } catch (Exception e) {
            throw new PlanningException(String.format("Failed to set SLA state: %s",
                    e.getMessage()), e);
        }
    }
}
