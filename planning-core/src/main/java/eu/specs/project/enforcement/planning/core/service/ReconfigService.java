package eu.specs.project.enforcement.planning.core.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.specs.datamodel.agreement.offer.AgreementOffer;
import eu.specs.datamodel.agreement.terms.GuaranteeTerm;
import eu.specs.datamodel.agreement.terms.Term;
import eu.specs.datamodel.common.SlaState;
import eu.specs.datamodel.enforcement.*;
import eu.specs.datamodel.sla.sdt.SLOType;
import eu.specs.project.enforcement.planning.core.client.ImplementationClient;
import eu.specs.project.enforcement.planning.core.client.MonipoliClient;
import eu.specs.project.enforcement.planning.core.client.SlaManagerClient;
import eu.specs.project.enforcement.planning.core.exceptions.PlanningException;
import eu.specs.project.enforcement.planning.core.processor.PlanningActivityProcessor;
import eu.specs.project.enforcement.planning.core.repository.PlanningActivityRepository;
import eu.specs.project.enforcement.planning.core.repository.ReconfigRepository;
import eu.specs.project.enforcement.planning.core.util.JsonDumper;
import eu.specsproject.core.slaplatform.auditing.client.AuditClient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.BadRequestException;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class ReconfigService {
    private static final Logger logger = LogManager.getLogger(ReconfigService.class);
    public static final int EXECUTOR_NUMBER_OF_THREADS = 3;
    private ExecutorService executorService = Executors.newFixedThreadPool(EXECUTOR_NUMBER_OF_THREADS);

    @Autowired
    private PlanningActivityService planningActivityService;

    @Autowired
    private ReconfigRepository reconfigRepository;

    @Autowired
    private ImplementationClient implementationClient;

    @Autowired
    private SlaManagerClient slaManagerClient;

    @Autowired
    private MonipoliClient monipoliClient;

    @Autowired
    private AuditClient auditClient;

    @Autowired
    private PlanningActivityRepository paRepository;

    @Autowired
    private PlanningActivityProcessor paProcessor;

    @Autowired
    private ObjectMapper objectMapper;

    public Reconfiguration findById(String reconfigId) {
        return reconfigRepository.findById(reconfigId);
    }

    public List<Reconfiguration> findAll(ReconfigsFilter filter) {
        return reconfigRepository.findAll(filter);
    }

    public Reconfiguration reconfigure(Reconfiguration reconfig) throws PlanningException {
        logger.debug("reconfigure() started:\n{}", JsonDumper.dump(reconfig));
        reconfig.setId(UUID.randomUUID().toString());
        reconfig.setCreationTime(new Date());
        reconfigRepository.save(reconfig);

        PlanningActivity planningActivity = planningActivityService.findBySlaId(reconfig.getSlaId());
        logger.debug("Found planning activity: {}", planningActivity.getId());

        switch (reconfig.getLabel()) {
            case "TERMINATE":
                terminateSla(planningActivity);
                break;
            case "UPDATE":
                implementRenegotiatedSla(planningActivity, reconfig);
                break;
            default:
                throw new BadRequestException("Invalid reconfiguration label: " + reconfig.getLabel());
        }

        return reconfig;
    }

    private void terminateSla(PlanningActivity planningActivity) throws PlanningException {
        logger.debug("terminateSla(paId={}) started.", planningActivity.getId());

        String slaId = planningActivity.getSlaId();

        auditClient.logComponentActivity("Planning", CompActivity.ComponentState.ACTIVATED, slaId, SlaState.TERMINATING);

        monipoliClient.removeMonipoliRules(slaId);

        implementationClient.deleteImplActivity(planningActivity.getImplActivityId());

        slaManagerClient.setSlaState(slaId, SlaState.TERMINATED);

        planningActivity.setState(PlanningActivity.Status.TERMINATED);
        paRepository.save(planningActivity);

        auditClient.logComponentActivity("Planning", CompActivity.ComponentState.ACTIVATED,
                planningActivity.getSlaId(), SlaState.TERMINATED);
    }

    private boolean implementRenegotiatedSla(final PlanningActivity planningActivity, Reconfiguration reconfig) throws PlanningException {
        logger.debug("implementRenegotiatedSla(paId={}) started.", planningActivity.getId());

        try {
            final ImplementationPlan implPlan = implementationClient.getImplPlan(planningActivity.getActivePlanId());
            if (implPlan == null) {
                throw new Exception(String.format("The Implementation plan %s cannot be found.",
                        planningActivity.getActivePlanId()));
            }
            AgreementOffer renegotiatedSla = slaManagerClient.retrieveSla(reconfig.getSlaId());

            final ImplementationPlan implPlanUpdated = objectMapper.readValue(
                    objectMapper.writeValueAsString(implPlan),
                    ImplementationPlan.class
            );

            Map<String, Slo> sloMap = new HashMap<>();
            for (Slo slo : implPlanUpdated.getSlos()) {
                sloMap.put(slo.getId(), slo);
            }
            List<Slo> changedSlos = new ArrayList<>();

            // update SLOs
            for (Term term : renegotiatedSla.getTerms().getAll().getAll()) {
                if (term instanceof GuaranteeTerm) {
                    GuaranteeTerm guaranteeTerm = (GuaranteeTerm) term;
                    for (SLOType sloSla : guaranteeTerm.getServiceLevelObjective().getCustomServiceLevel().getObjectiveList().getSLO()) {
                        String sloId = sloSla.getSLOID();
                        Slo.ImportanceLevel importanceLevel = Slo.ImportanceLevel.valueOf(sloSla.getImportanceWeight().value());
                        String operator = sloSla.getSLOexpression().getOneOpExpression().getOperator().name();
                        Object operand = sloSla.getSLOexpression().getOneOpExpression().getOperand();

                        Slo slo = sloMap.get(sloId);
                        if (slo == null) {
                            throw new UnsupportedOperationException(String.format(
                                    "SLO %s cannot be found in the implementation plan %s.", sloId, implPlan.getId()));
                        }

                        if (!Objects.equals(slo.getValue(), operand.toString())) {
                            changedSlos.add(slo);
                            slo.setImportanceLevel(importanceLevel);
                            slo.setOperator(operator);
                            slo.setValue(operand.toString());
                        }
                    }
                }
            }

            if (changedSlos.isEmpty()) {
                logger.info("No change has been detected in the renegotiated SLA.");
                return false;
            }

            // original SLOs
            Map<String, Slo> originalSloMap = new HashMap<>();
            for (Slo slo : implPlan.getSlos()) {
                originalSloMap.put(slo.getMetricId(), slo);
            }

            // affected capabilities
            Map<String, List<Slo>> capabilitySlosMapping = new HashMap<>();
            for (Slo slo : changedSlos) {
                String capability = slo.getCapability();
                if (!capabilitySlosMapping.containsKey(capability)) {
                    capabilitySlosMapping.put(capability, new ArrayList<Slo>());
                }
                capabilitySlosMapping.get(capability).add(slo);
            }

            // reconfigure implementation plan
            // TODO: this is just a mockup
            if (capabilitySlosMapping.containsKey("WEBPOOL")) {
                reconfigureWebPoolCapability(capabilitySlosMapping.get("WEBPOOL"), originalSloMap, implPlanUpdated);
            }

            if (logger.isTraceEnabled()) {
                logger.trace("Reconfiguration implementation plan:\n{}", JsonDumper.dump(implPlanUpdated));
            }

            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    paProcessor.processReconfigPlanningActivity(planningActivity, implPlanUpdated);
                }
            });
            logger.debug("processReconfigPlanningActivity job started.");

            logger.debug("implementRenegotiatedSla() finished successfully.");
            return true;

        } catch (Exception e) {
            logger.error("Failed to implement renegotiated SLA {}.", reconfig.getSlaId(), e);
            throw new PlanningException(String.format("Failed to implement renegotiated SLA %s.",
                    reconfig.getSlaId()));
        }
    }

    private void reconfigureWebPoolCapability(List<Slo> changedSlos, Map<String, Slo> originalSloMap,
                                              ImplementationPlan implPlan) throws PlanningException, IOException {
        Map<String, Slo> metricSloMapping = new HashMap<>();
        for (Slo slo : changedSlos) {
            metricSloMapping.put(slo.getMetricId(), slo);
        }

        if (metricSloMapping.containsKey("level_of_redundancy_m1")) {
            int newValue = Integer.parseInt(metricSloMapping.get("level_of_redundancy_m1").getValue());
            int oldValue = Integer.parseInt(originalSloMap.get("level_of_redundancy_m1").getValue());
            if (newValue < oldValue) {
                // remove excessive VMs
                int numberRemoved = 0;
                List<ImplementationPlan.Vm> vms = implPlan.getPools().get(0).getVms();
                for (int i = 0; i < vms.size(); i++) {
                    ImplementationPlan.Vm vm = vms.get(vms.size() - i - 1);
                    for (ImplementationPlan.Component component : vm.getComponents()) {
                        if (Objects.equals(component.getId(), "wp_apache") ||
                                Objects.equals(component.getId(), "wp_nginx")) {
                            vm.setComponents(null); // mark this VM for termination
                            numberRemoved++;
                            break;
                        }
                    }
                    logger.debug("Number of VMs to remove: {}", numberRemoved);
                    if (numberRemoved >= oldValue - newValue) {
                        break;
                    }
                }
            } else if (newValue > oldValue) {
                // add new VMs
                List<ImplementationPlan.Vm> vms = implPlan.getPools().get(0).getVms();
                ImplementationPlan.Vm vmToCopy = null;
                for (ImplementationPlan.Vm vm : vms) {
                    for (ImplementationPlan.Component component : vm.getComponents()) {
                        if (Objects.equals(component.getId(), "wp_apache") ||
                                Objects.equals(component.getId(), "wp_nginx")) {
                            vmToCopy = vm;
                            break;
                        }
                    }
                }

                if (vmToCopy == null) {
                    throw new PlanningException("Failed to update implementation plan.");
                }

                for (int i = 0; i < newValue - oldValue; i++) {
                    ImplementationPlan.Vm vmToAdd = objectMapper.readValue(
                            objectMapper.writeValueAsString(vmToCopy),
                            ImplementationPlan.Vm.class
                    );
                    vmToAdd.setVmSeqNum(vms.size() + i);
                    vmToAdd.setPublicIp(null);
                    for (ImplementationPlan.Component component : vmToAdd.getComponents()) {
                        component.setPrivateIps(null);
                    }
                    vms.add(vmToAdd);
                }
                logger.debug("Number of VMs to add: {}", newValue - oldValue);
            }
        }
    }

    public static class ReconfigsFilter {
        private String slaId;
        private Integer offset;
        private Integer limit;

        public String getSlaId() {
            return slaId;
        }

        public void setSlaId(String slaId) {
            this.slaId = slaId;
        }

        public Integer getOffset() {
            return offset;
        }

        public void setOffset(Integer offset) {
            this.offset = offset;
        }

        public Integer getLimit() {
            return limit;
        }

        public void setLimit(Integer limit) {
            this.limit = limit;
        }
    }
}
