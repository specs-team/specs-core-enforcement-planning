package eu.specs.project.enforcement.planning.core.repository;

import eu.specs.datamodel.enforcement.Reconfiguration;
import eu.specs.project.enforcement.planning.core.service.ReconfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ReconfigRepository {

    @Autowired
    MongoTemplate mongoTemplate;

    public Reconfiguration findById(String reconfigId) {
        return mongoTemplate.findById(reconfigId, Reconfiguration.class);
    }

    public List<Reconfiguration> findAll(ReconfigService.ReconfigsFilter filter) {
        Query query = new Query();
        if (filter.getSlaId() != null) {
            query.addCriteria(Criteria.where("slaId").is(filter.getSlaId()));
        }
        if (filter.getLimit() != null) {
            query.limit(filter.getLimit());
        }
        if (filter.getOffset() != null) {
            query.skip(filter.getOffset());
        }

        query.fields().include("id");
        return mongoTemplate.find(query, Reconfiguration.class);
    }

    public void save(Reconfiguration reconfig) {
        mongoTemplate.save(reconfig);
    }

    public void delete(Reconfiguration reconfig) {
        mongoTemplate.remove(reconfig);
    }
}
