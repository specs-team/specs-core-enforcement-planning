package eu.specs.project.enforcement.planning.core.repository;

import eu.specs.datamodel.enforcement.SupplyChain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SupplyChainRepository {

    @Autowired
    MongoTemplate mongoTemplate;

    public SupplyChain find(String supplyChainId) {
        return mongoTemplate.findById(supplyChainId, SupplyChain.class);
    }

    public List<SupplyChain> findByScaId(String scaId) {
        Query query = new Query().addCriteria(Criteria.where("scaId").is(scaId));
        return mongoTemplate.find(query, SupplyChain.class);
    }

    public List<SupplyChain> findBySlaId(String slaId) {
        Query query = new Query().addCriteria(Criteria.where("slaId").is(slaId));
        return mongoTemplate.find(query, SupplyChain.class);
    }

    public List<SupplyChain> getAll() {
        return mongoTemplate.findAll(SupplyChain.class);
    }

    public void save(SupplyChain supplyChain) {
        mongoTemplate.save(supplyChain);
    }

    public void delete(SupplyChain supplyChain) {
        mongoTemplate.remove(supplyChain);
    }
}
