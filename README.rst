.. contents:: Table of Contents

========
Overview
========

The SPECS Core Enforcement Planning component is involved in the SLA negotiation and SLA implementation phases. The main functionalities provided by the component are:

- generates valid supply chains according to end user’s security requirements
- prepares implementation plan according to a signed SLA and associated supply chain
- prepares a reaction plan to reconfigure target services after SLA renegotiation and SLA termination
- updates the monitoring policy. 

The following two sequence diagrams show the Planning component role at the SLA negotiation and implementation:

.. image:: https://bitbucket.org/specs-team/specs-core-enforcement-planning/raw/master/docs/images/impl_phase.png

**SLA implementation phase (initial implementation of an SLA)**

.. image:: https://bitbucket.org/specs-team/specs-core-enforcement-planning/raw/master/docs/images/impl_phase_reneg.png

**SLA implementation phase (after renegotiation or termination)**

More details about the component can be found in the deliverable 4.2.2 available  at `http://www.specs-project.eu/publications/public-deliverables/d4-2-2 <http://www.specs-project.eu/publications/public-deliverables/d4-2-2>`_.

============
Installation
============

Prerequisites:

- Java web container
- MongoDB
- Java 7

The Planning component is packaged as a web application archive (WAR) file with the name planning-api.war. The archive file can be built from source code or downloaded from the SPECS maven repository::

 https://nexus.services.ieat.ro/nexus/content/repositories/specs-snapshots

To build the Planning component from source code you need the Apache Maven 3 tool. First clone the project from the Bitbucket repository using a Git client::

 git clone git@bitbucket.org:specs-team/specs-core-enforcement-planning.git

then go into the specs-core-enforcement-planning directory and run::

 mvn package

The planning-api.war file is located in the planning-api/target directory.

The planning-api.war file has to be deployed to a Java web container. For example, to deploy the application to Apache Tomcat 7, just copy the war file to the Tomcat webapps directory::

 cp planning-api/target/planning-api.war /var/lib/tomcat7/webapps/

The application configuration is located in the file *planning.properties* in the Java properties format. The file contains the following configuration properties:

.. code-block:: jproperties

 sla-manager-api.address=https://localhost/sla-manager-api/sla_manager_rest_api
 service-manager-api.address=https://localhost/service-manager-api/cloud-sla
 implementation-api.address=https://localhost/implementation-api
 auditing-api.address=https://localhost/auditing
 monipoli-api.address=https://localhost/monipoli
 mongodb.host=localhost
 mongodb.port=27017
 mongodb.database=enforcement-planning

Make the necessary changes and restart the web container for changes to take effect. The Planning API should now be available at::

 https://<host>:<port>/planning-api

======
Notice
======

This product includes software developed at "XLAB d.o.o, Slovenia", as part of the "SPECS - Secure Provisioning of Cloud Services based on SLA Management" research project (an EC FP7-ICT Grant, agreement 610795).

- http://www.specs-project.eu/
- http://www.xlab.si/

Developers:

- Jolanda Modic, jolanda.modic@xlab.si
- Damjan Murn, damjan.murn@xlab.si

Copyright:

.. code-block:: 

 Copyright 2013-2015, XLAB d.o.o, Slovenia
    http://www.xlab.si

 SPECS Core Enforcement Planning is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License, version 3,
 as published by the Free Software Foundation.

 SPECS Core Enforcement Planning is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.