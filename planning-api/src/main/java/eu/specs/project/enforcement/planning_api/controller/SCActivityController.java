package eu.specs.project.enforcement.planning_api.controller;

import eu.specs.datamodel.common.ResourceCollection;
import eu.specs.datamodel.enforcement.SupplyChain;
import eu.specs.datamodel.enforcement.SupplyChainActivity;
import eu.specs.project.enforcement.planning.core.service.SCActivityService;
import eu.specs.project.enforcement.planning.core.util.JsonDumper;
import eu.specs.project.enforcement.planning_api.exception.ResourceNotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/sc-activities")
public class SCActivityController {
    private static final Logger logger = LogManager.getLogger(SCActivityController.class);

    @Autowired
    private SCActivityService scActivityService;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.CREATED)
    public ResponseEntity<SupplyChainActivity> buildSupplyChains(@RequestBody SupplyChainActivity inputSCA,
                                                                 HttpServletRequest request) {
        if (logger.isTraceEnabled()) {
            logger.trace("buildSupplyChains: request received with input data:\n{}", JsonDumper.dump(inputSCA));
        }

        SupplyChainActivity sca = scActivityService.buildSupplyChains(inputSCA);

        if (logger.isTraceEnabled()) {
            logger.trace("Supply chain activity finished:\n{}", JsonDumper.dump(sca));
        }

        URI location = URI.create(request.getRequestURL().append("/").append(sca.getId()).toString());
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(location);
        return new ResponseEntity<SupplyChainActivity>(sca, headers, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResourceCollection getCollection(HttpServletRequest request) {
        List<SupplyChainActivity> scaList = scActivityService.getAll();
        ResourceCollection collection = new ResourceCollection();
        collection.setResource("sc-activity");
        collection.setTotal(scaList.size());
        collection.setMembers(scaList.size());

        for (SupplyChainActivity scActivity : scaList) {
            String uri = request.getRequestURL() + "/" + scActivity.getId();
            collection.addItem(new ResourceCollection.Item(scActivity.getId(), uri));
        }
        return collection;
    }

    @RequestMapping(value = "/{scaId}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public SupplyChainActivity getSCA(@PathVariable("scaId") String scaId) {
        SupplyChainActivity sca = scActivityService.getSCA(scaId);
        if (sca == null) {
            throw new ResourceNotFoundException(String.format("The supply chain activity %s cannot be found.", scaId));
        } else {
            return sca;
        }
    }

    @RequestMapping(value = "/{scaId}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteSCA(@PathVariable("scaId") String scaId) {
        SupplyChainActivity sca = scActivityService.getSCA(scaId);
        if (sca == null) {
            throw new ResourceNotFoundException(String.format("The supply chain activity %s cannot be found.", scaId));
        } else {
            scActivityService.deleteSCA(sca);
        }
    }

    @RequestMapping(value = "/{scaId}/state", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public Map getSCAState(@PathVariable("scaId") String scaId) {
        SupplyChainActivity sca = scActivityService.getSCA(scaId);
        if (sca == null) {
            throw new ResourceNotFoundException(String.format("The supply chain activity %s cannot be found.", scaId));
        } else {
            Map map = new HashMap();
            map.put("state", sca.getState().name());
            return map;
        }
    }

    @RequestMapping(value = "/{scaId}/supply-chains", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public List<SupplyChain> getSCASupplyChains(@PathVariable("scaId") String scaId) {
        SupplyChainActivity sca = scActivityService.getSCA(scaId);
        if (sca == null) {
            throw new ResourceNotFoundException(String.format("The supply chain activity %s cannot be found.", scaId));
        } else {
            return scActivityService.getSupplyChains(sca);
        }
    }
}
