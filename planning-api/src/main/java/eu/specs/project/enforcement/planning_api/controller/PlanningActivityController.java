package eu.specs.project.enforcement.planning_api.controller;

import eu.specs.datamodel.common.ResourceCollection;
import eu.specs.datamodel.enforcement.PlanningActivity;
import eu.specs.datamodel.enforcement.SupplyChain;
import eu.specs.project.enforcement.planning.core.exceptions.PlanningException;
import eu.specs.project.enforcement.planning.core.repository.SupplyChainRepository;
import eu.specs.project.enforcement.planning.core.service.PlanningActivityService;
import eu.specs.project.enforcement.planning.core.util.JsonDumper;
import eu.specs.project.enforcement.planning_api.exception.ResourceNotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "/plan-activities")
public class PlanningActivityController {
    private static final Logger logger = LogManager.getLogger(PlanningActivityController.class);

    @Autowired
    private PlanningActivityService paService;

    @Autowired
    private SupplyChainRepository scRepository;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.CREATED)
    public ResponseEntity<PlanningActivity> createPlanningActivity(@RequestBody Map<String, String> data,
                                                                   HttpServletRequest request) throws PlanningException {
        if (logger.isTraceEnabled()) {
            logger.trace("createPlanningActivity() started. Data: " + JsonDumper.dump(data));
        }
        SupplyChain supplyChain = null;

        if (data.containsKey("supply_chain_id")) {
            String supplyChainId = data.get("supply_chain_id");
            supplyChain = scRepository.find(supplyChainId);
            if (supplyChain == null) {
                throw new HttpClientErrorException(HttpStatus.NOT_FOUND, "Supply chain cannot be found.");
            }

        } else if (data.containsKey("sla_id")) {
            String slaId = data.get("sla_id");
            List<SupplyChain> supplyChains = scRepository.findBySlaId(slaId);
            if (supplyChains.isEmpty()) {
                throw new HttpClientErrorException(HttpStatus.NOT_FOUND, "No corresponding supply chain found for the SLA template " + slaId);
            } else if (supplyChains.size() > 1) {
                throw new HttpClientErrorException(HttpStatus.NOT_FOUND, "More than one supply chain found for the SLA template " + slaId);
            }

            supplyChain = supplyChains.get(0);

        } else {
            throw new HttpClientErrorException(HttpStatus.NOT_FOUND, "Missing parameter supply_chain_id or sla_id.");
        }

        PlanningActivity planningActivity = paService.createPlanningActivity(supplyChain);

        URI location = URI.create(request.getRequestURL().append("/").append(planningActivity.getId()).toString());
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(location);
        logger.trace("createPlanningActivity() finished successfully. Planning activity ID: {}", planningActivity.getId());
        return new ResponseEntity<>(planningActivity, headers, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResourceCollection getPlanningActivities(
            @RequestParam(value = "slaId", required = false) String slaId,
            @RequestParam(value = "state", required = false) PlanningActivity.Status state,
            @RequestParam(value = "offset", required = false) Integer offset,
            @RequestParam(value = "limit", required = false) Integer limit,
            HttpServletRequest request) throws PlanningException {

        PlanningActivityService.PlanningActivityFilter filter = new PlanningActivityService.PlanningActivityFilter();
        filter.setSlaId(slaId);
        filter.setState(state);
        filter.setOffset(offset);
        filter.setLimit(limit);

        List<PlanningActivity> planningActivities = paService.find(filter);

        ResourceCollection collection = new ResourceCollection();
        collection.setResource("plan-activity");
        collection.setTotal(planningActivities.size());
        collection.setMembers(planningActivities.size());

        for (PlanningActivity planningActivity : planningActivities) {
            String uri = request.getRequestURL() + "/" + planningActivity.getId();
            collection.addItem(new ResourceCollection.Item(planningActivity.getId(), uri));
        }
        return collection;
    }

    @RequestMapping(value = "/{paId}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public PlanningActivity getPlanningActivity(@PathVariable("paId") String paId) {
        PlanningActivity planningActivity = paService.getPlanningActivity(paId);
        if (planningActivity == null) {
            throw new ResourceNotFoundException("The Planning activity cannot be found.");
        } else {
            return planningActivity;
        }
    }

    @RequestMapping(value = "/{paId}/status", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public Map<String, String> getPlanningActivityStatus(@PathVariable("paId") String paId) {
        PlanningActivity planningActivity = paService.getPlanningActivity(paId);
        if (planningActivity == null) {
            throw new ResourceNotFoundException(String.format("The Planning activity %s cannot be found.", paId));
        } else {
            Map<String, String> resp = new HashMap<>();
            resp.put("status", planningActivity.getState().name());
            return resp;
        }
    }

    @RequestMapping(value = "/{paId}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deletePlanningActivity(@PathVariable("paId") String paId) throws PlanningException {
        PlanningActivity planningActivity = paService.getPlanningActivity(paId);
        if (planningActivity == null) {
            throw new ResourceNotFoundException(String.format("The Planning activity %s cannot be found.", paId));
        } else {
            paService.deletePlanningActivity(planningActivity);
        }
    }

    @RequestMapping(value = "/{paId}/active", method = RequestMethod.GET, produces = {MediaType.TEXT_PLAIN_VALUE})
    @ResponseBody
    public String getActiveImplPlanId(@PathVariable("paId") String paId) {
        PlanningActivity planningActivity = paService.getPlanningActivity(paId);
        if (planningActivity == null) {
            throw new ResourceNotFoundException("The Planning activity cannot be found.");
        } else {
            return planningActivity.getActivePlanId();
        }
    }
}
