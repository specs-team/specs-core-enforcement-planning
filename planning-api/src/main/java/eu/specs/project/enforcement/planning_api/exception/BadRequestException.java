package eu.specs.project.enforcement.planning_api.exception;

public class BadRequestException extends Exception {

    public BadRequestException(String msg) {
        super(msg);
    }
}
