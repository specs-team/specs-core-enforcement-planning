package eu.specs.project.enforcement.planning_api.controller;

import eu.specs.datamodel.enforcement.ImplementationPlan;
import eu.specs.project.enforcement.planning.core.client.ImplementationClient;
import eu.specs.project.enforcement.planning.core.exceptions.PlanningException;
import eu.specs.project.enforcement.planning_api.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/impl-plans")
public class ImplementationPlanController {

    @Autowired
    private ImplementationClient implementationClient;

    @RequestMapping(value = "/{implPlanId}", method = RequestMethod.GET)
    @ResponseBody
    public ImplementationPlan getImplementationPlan(@PathVariable("implPlanId") String implPlanId)
            throws PlanningException {
        ImplementationPlan implementationPlan = implementationClient.getImplPlan(implPlanId);
        if (implementationPlan == null) {
            throw new ResourceNotFoundException(String.format("The Implementation plan %s cannot be found.", implPlanId));
        } else {
            return implementationPlan;
        }
    }
}
