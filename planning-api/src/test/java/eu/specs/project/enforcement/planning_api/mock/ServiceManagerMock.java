package eu.specs.project.enforcement.planning_api.mock;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Resources;
import eu.specs.datamodel.enforcement.SecurityMechanism;

import java.io.IOException;
import java.nio.charset.Charset;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

public class ServiceManagerMock {

    private ObjectMapper objectMapper;

    public ServiceManagerMock() throws Exception {
        objectMapper = new ObjectMapper();
    }

    public void init() throws IOException {

        String mechanismSvaJson = Resources.toString(
                this.getClass().getResource("/mechanism-sva.json"), Charset.forName("UTF-8"));
        SecurityMechanism mechanismSva = objectMapper.readValue(mechanismSvaJson, SecurityMechanism.class);

        stubFor(get(urlEqualTo("/service-manager/cloud-sla/security-mechanisms/" + mechanismSva.getId()))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(mechanismSvaJson)));

        String mechanismWebPoolJson = Resources.toString(
                this.getClass().getResource("/mechanism-webpool.json"), Charset.forName("UTF-8"));
        SecurityMechanism mechanismWebPool = objectMapper.readValue(mechanismWebPoolJson, SecurityMechanism.class);

        stubFor(get(urlEqualTo("/service-manager/cloud-sla/security-mechanisms/" + mechanismWebPool.getId()))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody(mechanismWebPoolJson)));
    }
}
