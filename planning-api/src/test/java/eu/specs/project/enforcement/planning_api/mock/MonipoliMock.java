package eu.specs.project.enforcement.planning_api.mock;

import javax.ws.rs.core.MediaType;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

public class MonipoliMock {
    private static String SLA_ID = "56BB164F001A8A2CA67C7AAF";

    public void init() {

        stubFor(post(urlPathEqualTo("/monipoli"))
                .withHeader("Content-Type", equalTo(MediaType.APPLICATION_XML))
                .willReturn(aResponse()
                        .withStatus(200)));

        stubFor(delete(urlPathEqualTo("/monipoli"))
                .willReturn(aResponse()
                        .withStatus(204)));
    }
}
