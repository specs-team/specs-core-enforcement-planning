package eu.specs.project.enforcement.planning_api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import eu.specs.datamodel.enforcement.PlanningActivity;
import eu.specs.datamodel.enforcement.Reconfiguration;
import eu.specs.project.enforcement.planning.core.repository.PlanningActivityRepository;
import eu.specs.project.enforcement.planning_api.TestParent;
import eu.specs.project.enforcement.planning_api.mock.ImplementationMock;
import eu.specs.project.enforcement.planning_api.mock.MonipoliMock;
import eu.specs.project.enforcement.planning_api.mock.SlaManagerMock;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("classpath:planning-api-context-test.xml")
public class ReconfigApiTest extends TestParent {
    private static final Logger logger = LogManager.getLogger(ReconfigApiTest.class);

    private static String SLA_ID = "56BB164F001A8A2CA67C7AAF";

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(Integer.parseInt(System.getProperty("wiremock.port")));

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private PlanningActivityRepository planningActivityRepository;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(this.wac).build();
    }

    @Test
    public void testTermination() throws Exception {
        logger.debug("testTermination() started.");

        new SlaManagerMock().init();
        new ImplementationMock().init();
        new MonipoliMock().init();

        PlanningActivity planningActivity = objectMapper.readValue(
                this.getClass().getResourceAsStream("/planning-activity.json"), PlanningActivity.class);
        planningActivityRepository.save(planningActivity);

        Reconfiguration reconfigData = new Reconfiguration();
        reconfigData.setSlaId(SLA_ID);
        reconfigData.setLabel("TERMINATE");

        MvcResult result = mockMvc.perform(post("/reconfigs")
                .content(objectMapper.writeValueAsString(reconfigData)).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();

        Reconfiguration reconfig = objectMapper.readValue(
                result.getResponse().getContentAsString(), Reconfiguration.class);

        assertNotNull(reconfig.getId());

        mockMvc.perform(get("/reconfigs/{0}", reconfig.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.reconfig_id", is(reconfig.getId())));

        mockMvc.perform(get("/reconfigs"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.total", is(1)));
    }
}